package com.example.tugas2_faizrifqi_215150407111040;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void goToSecondActivity(View view) {
        EditText usernameEditText = findViewById(R.id.editTextTextEmailAddress);
        EditText passwordEditText = findViewById(R.id.editTextTextPassword);

        String inputUsername = usernameEditText.getText().toString();
        String inputPassword = passwordEditText.getText().toString();

        String expectedUsername = "Faiz Rifqi";
        String expectedPassword = "215150407111040";

        if (inputUsername.equals(expectedUsername) && inputPassword.equals(expectedPassword)) {
            Intent intent = new Intent(this, after_login.class);
            intent.putExtra("username", inputUsername);
            intent.putExtra("password", inputPassword);
            startActivity(intent);
        } else {
//             TextView: errorTextView.setText("Invalid username or password");
            Toast.makeText(MainActivity.this, "Invalid username or password", Toast.LENGTH_SHORT).show();

        }
    }
}
