package com.example.tugas2_faizrifqi_215150407111040;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class after_login extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_after_login);
        String username = getIntent().getStringExtra("username");
        String password = getIntent().getStringExtra("password");
        TextView textViewUsername = findViewById(R.id.textView);
        TextView textViewPassword = findViewById(R.id.textView2);
        textViewUsername.setText(username);
        textViewPassword.setText(password);
    }
}